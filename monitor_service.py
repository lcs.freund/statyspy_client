#!venv/bin/python

import os
from components.statyspy_monitor import monitor
from statys_logging.exception_logger import logger
from statys_logging.exception_decor import exception
from persistence import persister
import json
import sys
import argparse
import time
from pystray import MenuItem, Menu
import pystray
from PIL import Image

parser = argparse.ArgumentParser()
parser.add_argument("-R", "--register", help="Register Client to Server",
                    action="store_true")
parser.add_argument("-U", "--unregister", help="Unregister Client from Server",
                    action="store_true")

parser.add_argument("-p", "--reset", help="reset persistence",
                    action="store_true")
INTERVAL = 1
MONITOR = monitor()
RUN = False


def exit_action(icn):
    logger.info("Exiting Via Tray")
    icn.visible = False
    logger.info("Goodbye")
    os._exit(0)



@exception(logger)
def main(run: bool):
    MONITOR.connect()
    logger.info("Monitor Started")
    while run:
        try:
            response = MONITOR.send_payload()
            logger.info("Payload Sent, Response: " + str(json.loads(response)))
            time.sleep(INTERVAL)
        except Exception as e:  # TODO Error handling, this throws errors when server is down
            print(e)
    print("Closing")

@exception(logger)
def reset():
    persister.reset_persister()


def setup(icn):
    icn.visible = True
    icn.menu = Menu(
        MenuItem('Exit', lambda: exit_action(icn)),
    )
    main(icn.visible)


if __name__ == '__main__':
    MONITOR.read_cfg()

    INTERVAL = MONITOR.update_interval()

    args = parser.parse_args()
    if args.register:
        logger.info("Registering Client")
        MONITOR.connect()
        msg = MONITOR.register_client()
        logger.info(msg)

    elif args.unregister:
        logger.info("Unregistering Client")
        MONITOR.connect()
        msg = MONITOR.unregister_client()
        logger.info(msg)

    elif args.reset:
        logger.info("Resetting Persisctence")
        persister.reset_persister()

    else:
        logger.info("Starting Monitor")
        try:
            image = Image.open("staty.png")

            icon = pystray.Icon(icon=image, name="Statysspy", title="Statysspy Monitor")
            icon.run(setup)

            print("closing now")
            # Gets called when main exits

        except Exception as e:
            logger.error("Error Occured :(")
            logger.error(str(e))
