import unittest
import psutil

from components.statyspy_base import SystemApi


if psutil.LINUX:
    pass
else:
    pass


class ApiUnitTest(unittest.TestCase):
    def setUp(self):
        self.api = SystemApi()

    def tearDown(self):
        pass

    def test_output_of_memory_consumption(self):
        self.api.cpu_consumption_top(10)
    
    def test_output_of_cpu_consumption(self):
        self.api.memory_consumption_top(10)
        


if __name__ == '__main__':
    unittest.main()


