import unittest
import psutil
from components.statyspy_base import ServiceApi, SystemApi
TESTABLE_RUNNING_SERVICE_NAME= ""
TESTABLE_STOPPED_SERVICE_NAME = 'TapiSrv'

if psutil.LINUX:
    TESTABLE_RUNNING_SERVICE_NAME = '/sbin/init'
    
else:
    TESTABLE_RUNNING_SERVICE_NAME = 'TimeBrokerSvc'



class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.running_api = ServiceApi(TESTABLE_RUNNING_SERVICE_NAME)
        self.not_running_api = ServiceApi(TESTABLE_STOPPED_SERVICE_NAME)
        self.test_system_api = SystemApi()

    def tearDown(self):
        pass

    def test_ServiceApi_is_service_running_valid_return_value(self):
        self.assertEqual(self.running_api.is_service_running(), True)
        self.assertEqual(self.not_running_api.is_service_running(), False)

    def test_ServiceApi_get_service_status_valid_return_value(self):
        self.assertEqual(self.running_api.get_service_status(), "running")
        self.assertEqual(self.not_running_api.get_service_status(), "stopped")

    def test_SystemApi_Test_Output(self):
        """ The output must be human validated """
        print(self.test_system_api.get_report())
        print(self.test_system_api.get_cpu_data())


    def test_SystemApi_Test_Aux(self):
        """ The output must be human validated """
        print(self.test_system_api.collect_auxillary_data())
        


    # def test_SystemApi_Test_Report(self):
    #     print("Check if Hostname {} and IP {} are correct".format(str(socket.gethostname()),str(ipgetter.myip())))
    #     self.assertEqual(
    #         str(socket.gethostname()),
    #         self.test_system_api.get_report()['host_name']
    #     )
    #     ip_address = ipgetter.myip()
    #     self.assertEqual(
    #         ip_address,
    #         self.test_system_api.get_report()['ip_adress']
    #     )


if __name__ == '__main__':
    unittest.main()
