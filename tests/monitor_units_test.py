import unittest
import psutil

from components.statyspy_monitor import monitor


if psutil.LINUX:
    pass
else:
    pass


class MonitorTestCase(unittest.TestCase):
    def setUp(self):
        self.m = monitor()
        self.m.read_cfg()
        self.m.connect()
        self.m.register_client()

    def tearDown(self):
        self.m.unregister_client()


    def test_monitor_send_payload_succeeds(self):
        print(self.m.updated_payload())
        self.m.send_payload()


if __name__ == '__main__':
    unittest.main()


