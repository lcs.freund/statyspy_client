# Staty Spy Monitor

This is the Monitor Application to the StatysSpy Push Monitoring Software

# Usage:
Edit config.json to your needs:

```json
{
  "statyspy_username":"username", 
  "statyspy_password":"password",
  "statyspy_server":"[your statyspy server url]",
  "services":[
        "a_service_by_name",
        "another_service_by_name"
  ],
  "update_interval":5
}
```
Register the client to your server by invoking `monitor_service.py -R`. You can unregister it again with the `-U` flag.
After unregistering you should reset the monitor by invoking `monitor_service.py -p`, otherwise further registering attempts will fail.
Run the agent by just executing the script and putting it in the background or make it startup when booting, for example via systemd.

#### You can run Server and client for testing purposes on the same machine if you like.

## Windows Exe specific instructions
The Monitor relies on the following structure:

```
Root
    /persistence
        persist.json
    monitor_service.exe
    config.json
```


### Installation From Source
You can "compile" an exe of this script which is executeable under windows without a python installation required utilizing pyinstaller. 

For this to work you also need pip and virtualenv. If you are doing this on windows,
i would recommend using pycharm and its virtual environment features.

The following steps should work on most linux distributions:

```bash
cd path/to/repo
pip install virtualenv
virtualenv venv
./venv/bin/activate
# you can skip the above and just do the last 3 steps with pycharm virtual environment
pip install -r requirements.txt
pip install pyinstaller
pyinstaller --onefile monitor_service.py
```

The resulting exe file can be called with the same flags the python script can be called with





