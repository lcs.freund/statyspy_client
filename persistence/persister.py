import json

PERSISTENCE_FILEPATH = "persistence/persist.json"

def reset_persister():
    with open(PERSISTENCE_FILEPATH, "r") as jsonFile:
        data = json.load(jsonFile)
        data["client_id"] = 'None'

    with open(PERSISTENCE_FILEPATH, "w") as jsonFile:    
        json.dump(data, jsonFile)
        
        


def already_persisted():
    """ determines if there is a client registered already """
    with open(PERSISTENCE_FILEPATH, "r") as jsonFile:
        data = json.load(jsonFile)
        return data["client_id"] != 'None'


def get_registered_client_id():
    """ returns the registered clients id"""
    with open(PERSISTENCE_FILEPATH, "r") as jsonFile:
        data = json.load(jsonFile)
        return data['client_id']


def persist_client_registration(client_id):
    """ remember the client id in persistence.json """
    with open(PERSISTENCE_FILEPATH, "r") as jsonFile:
        data = json.load(jsonFile)

    previous_id = data["client_id"]
    if previous_id == "None":
        data["client_id"] = client_id
    elif previous_id == client_id:
        print("Client ID already persisted")

    with open(PERSISTENCE_FILEPATH, "w") as jsonFile:
        json.dump(data, jsonFile)
