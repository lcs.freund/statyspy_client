import json
from statys_logging.exception_logger import logger
from statys_logging.exception_decor import exception
from typing import Dict, Any

from components.statyspy_api_wrapper import StatySpy_ApiWrapper as RestApi
from components.statyspy_base import SystemApi, ServiceApi
from persistence.persister import persist_client_registration, already_persisted, get_registered_client_id, reset_persister


class monitor:
    def __init__(self):
        self.connection_established = False
        self.session = {}
        self.system_api = SystemApi()
        self.service_apis=list()
        self.config = dict()

    @exception(logger)
    def connect(self):
        self.session['API'] = RestApi(
            self.config["statyspy_username"],
            self.config["statyspy_password"],
            self.config["statyspy_server"]
        )

        status_code = self.session['API'].login_session()

        if not status_code == 201:
            logger.info("Could not connect to server. Status Code: {}".format(status_code))
        else:
            logger.info(status_code)
            logger.info("connection established")
            self.connection_established = True

    def register_client(self):
        response = self.session['API'].register_client(self.updated_payload())
        logger.info(response)
        logger.info(response['message'])
        if "created_client_id" in response:
            persist_client_registration(response['created_client_id'])
            logger.info("Persisted")

    def unregister_client(self):
        """ Unregisters a client, if already registered. """
        if already_persisted():
            response = self.session['API'].remove_client(get_registered_client_id())
            reset_persister()
            logger.info("Client unregistered and persistence reset")
            return response

        return "Client was not registered"

    def updated_payload(self):
        """ returns an updated payload """
        report = self.system_api.get_report()
        services = list()
        for service in self.service_apis:
            services.append(service.to_dict())

        report['services'] = services

        return report

    def update_interval(self):
        return self.config["update_interval"]

    @exception(logger)
    def send_payload(self):
        """ Sends current stats to the server. client must be registered first """
        if already_persisted():
            client_id = get_registered_client_id()
        else:
            return "You must register this client first."

        try: # there has been some unnecessary code here :O updated payload does all that was here ...
            return self.session['API'].update_client_data(client_id, self.updated_payload())
        except Exception:
            logger.info("Update Failed")

    def read_cfg(self):
        """ cache the config and create service api objects """
        with open('config.json') as target:

            self.config = json.loads(target.read())
            # print connection information to console
            logger.info("{}\n{}\n{}\n{}".format(
                "username: " + self.config["statyspy_username"],
                "statyspy SERVER: " + self.config["statyspy_server"],
                "SERVICES: " + str(self.config["services"]),
                "Interval: " + str(self.config["update_interval"])
            ))
            # initialize service apis

            self.service_apis = list()
            for service in self.config["services"]:
                self.service_apis.append(ServiceApi(service))


