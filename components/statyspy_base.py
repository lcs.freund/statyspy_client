import psutil
import ipgetter
import socket
import platform
import os
import json
import time

OS = platform.system()

def linux_service_running(name):
    if OS != "Linux":
        print("Cant get a Linux Service on a non-Linux Operation System")
        return None

    tmp = os.popen("ps -Af").read()
    return name in tmp




class ServiceApi:
    def __init__(self, service_name: str):
        assert isinstance(service_name, str)
        if service_name is not "":
            self.service_name = service_name
        else:
            print("empty service name")

    def get_service(self):
        service = None
        try:
            if OS == "Windows":
                service = psutil.win_service_get(self.service_name)
                service = service.as_dict()
            elif OS == "Linux":
                running ="running" if linux_service_running(self.service_name) else "stopped"

                service = {
                    'status': running
                }

        except Exception as ex:
            print(str(ex))

        return service

    def is_service_running(self):
        service = self.get_service()
        if service and service['status'] == 'running':
            return True

        return False

    def get_service_status(self):
        try:
            return str(self.get_service()['status'])
        except Exception:
            return "Service could not be found"
    
    def to_dict(self):
        return{
            'name':self.service_name,
            'status':self.get_service_status()
        }


class SystemApi:
    def __init__(self):
        try:
            self.ip_address = ipgetter.myip()
            self.hostname = socket.gethostname()
        except Exception as e:
            print(str(e))

    def get_disk_data(self):
        total = psutil.disk_usage(".").total
        free = psutil.disk_usage(".").free
        return round(free / total, 2)

    @staticmethod
    def get_cpu_data():
        return psutil.cpu_percent(interval=1)

    @staticmethod
    def get_memory_data():
        return psutil.virtual_memory().percent

    def memory_consumption_top(self, amount):
        li = [(p.pid, p.info) for p in sorted(
            psutil.process_iter(attrs=[
                'name',
                'memory_percent']),
            key=lambda p: p.info['memory_percent'],reverse=False)][-amount:]

        ret = []
        for item in li:
            ret.append({
                'name':item[1]['name'],
                'pid':item[0],
                'percent':item[1]['memory_percent']
            })            
        return ret

    def cpu_consumption_top(self, amount):
        li = [(p.pid, p.info['name'], sum(p.info['cpu_times'])) for p in sorted(
            psutil.process_iter(attrs=[
                'name',
                'cpu_times']),
            key=lambda p: sum(p.info['cpu_times'][:2]),reverse=False)][-amount:]

        ret = []
        for item in li:
            ret.append({
                'name': item[1],
                'pid':item[0],
                'percent':item[2]
            })


        return ret

    def poll_network_rates(self, interval):
        tot_before = psutil.net_io_counters()
        time.sleep(interval)
        tot_after = psutil.net_io_counters()
        return (tot_before, tot_after)

    def get_network_rate(self):
        totals = self.poll_network_rates(1)
        rates = {
            'up': totals[1].bytes_sent - totals[0].bytes_sent,
            'down': totals[1].bytes_recv - totals[0].bytes_recv
        }
        return rates

    def collect_auxillary_data(self):
        aux = {
            'memory_consumption': self.memory_consumption_top(10),
            'cpu_consumption': self.cpu_consumption_top(10),
            'network_rates': self.get_network_rate()
        }

        return json.dumps(aux)

    def get_report(self) -> dict:
        return {
            "name": self.hostname,
            "ip_address": self.ip_address,
            "cpu_load": self.get_cpu_data(),
            "memory_load": self.get_memory_data(),
            "harddisk_load": self.get_disk_data(),
            "auxillary_data": self.collect_auxillary_data()
        }


