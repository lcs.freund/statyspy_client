import requests
import json

FAKE_UPDATE_DATA_A = {
    "name":"GANDOLF",
    "cpu_load":1.0,
    "memory_load":1.0,
    "harddisk_load":1.0
}

FAKE_UPDATE_DATA_B = {
    "name": "GANDOEF",
    "cpu_load": 7.0,
    "memory_load": 8.0,
    "harddisk_load": 9.0
}


class StatySpy_ApiWrapper:
    """ Object representing a session to the rest api """
    def __init__(self, user, password, server):
        """ Constructor: saves login information """
        self.LOGIN_ROUTE = "api/login"
        #self.READ_ROUTE = "statyspy_clientview/api/read"
        self.UPDATE_ROUTE = "api/clients/update/"
        self.REGISTER_ROUTE = "api/clients/register"
        self.UNREGISTER_ROUTE = "api/clients/unregister"


        self.login = {
            "username": user,
            "password": password
        }
        self.server = server
        self.connection_established = False
        self.cookie = dict()
        self.api_login_url = server + self.LOGIN_ROUTE
        # self.api_clients_read_url = server + self.READ_ROUTE
        self.api_clients_update_url = server + self.UPDATE_ROUTE
        self.api_clients_register_url = server + self.REGISTER_ROUTE
        self.api_clients_unregister_url = server + self.UNREGISTER_ROUTE

    def login_session(self):
        """ Establishes an authorized session """
        response = requests.post(self.api_login_url, json=self.login)

        if response.status_code == 201:
            self.cookie = response.cookies
            self.connection_established = True

        return response.status_code

    def get_client_list(self):
        """ get a list of clients from server ( not implemented )"""
        pass
        # response = requests.get(self.api_clients_read_url, cookies=self.cookie)
        # return response.json()

    def update_client_data(self, client_id, data):
        """ sends updated client data to the server """

        response = requests.put(self.api_clients_update_url + str(client_id), json=json.dumps(data), cookies=self.cookie)
        return response.content

    def register_client(self, data):
        """ registers a client and returns the response which includes the new clients id ( see server side ) """
        response = requests.post(self.api_clients_register_url, json=json.dumps(data), cookies=self.cookie)
        return response.json()

    def remove_client(self, client_id):
        """ unregisters a client from the api """
        response = requests.delete(self.api_clients_unregister_url+"/{}".format(client_id), cookies=self.cookie)
        return response.content

    def __repr__(self):
        return "|Api Session| Server: {}, User: {}, Connection Established? {}".format(
            self.server, self.login['username'], self.connection_established
        )
